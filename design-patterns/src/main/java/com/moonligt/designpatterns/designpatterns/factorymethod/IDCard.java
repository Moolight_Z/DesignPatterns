package com.moonligt.designpatterns.designpatterns.factorymethod;

/**
 * @ClassName IDCard
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:33
 * @Version V1.0
 **/
public class IDCard extends Product {

	private String owner;
	
	public IDCard (String owner) {
		System.out.println("create " + owner + " idcard ");
		this.owner = owner;
	}
	
	@Override
	public void use() {
		System.out.println("use " + owner + " idcard");
	}

}

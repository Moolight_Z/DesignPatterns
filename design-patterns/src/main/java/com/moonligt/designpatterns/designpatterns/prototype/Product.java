package com.moonligt.designpatterns.designpatterns.prototype;

/**
 * @ClassName Product
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 12:44
 * @Version V1.0
 **/
public interface Product extends Cloneable {
    void use();
    Product createClone();
}

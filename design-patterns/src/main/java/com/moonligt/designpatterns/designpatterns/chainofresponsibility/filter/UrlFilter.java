package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

/**
 * @ClassName UrlFilter
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 15:57
 * @Version V1.0
 **/
public class UrlFilter implements Filter {
    @Override
    public boolean doFilter(Req req, Resp resp, FilterChain chain) {
        String reqMsg = req.getReqMsg();
        reqMsg = reqMsg.replaceAll("http:", "").replaceAll("https:", "");
        req.setReqMsg(reqMsg + " - urlFilter");

        chain.doFilter(req, resp, chain);

        resp.setRespMsg(resp.getRespMsg() + " - urlFilter");

        return true;
    }
}

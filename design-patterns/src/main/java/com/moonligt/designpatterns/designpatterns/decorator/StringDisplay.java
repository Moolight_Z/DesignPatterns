package com.moonligt.designpatterns.designpatterns.decorator;

/**
 * @ClassName StringDisplay
 * @Description: 实际被装饰物
 * @Author Moonlight
 * @Date 2020/5/16 20:31
 * @Version V1.0
 **/
public class StringDisplay extends Display {
    private String string;

    public StringDisplay(String str){
        this.string = str;
    }

    @Override
    public int getColumns() {
        return this.string.length();
    }

    @Override
    public int getRows() {
        return 1;
    }

    @Override
    public String getRowText(int row) {
        return row == 0 ? this.string : null;
    }
}

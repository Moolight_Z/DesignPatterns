package com.moonligt.designpatterns.designpatterns.abstractfactory.tablefactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.Link;

/**
 * @ClassName TableLink
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:06
 * @Version V1.0
 **/
public class TableLink extends Link {
    public TableLink(String caption, String url) {
        super(caption, url);
    }

    @Override
    public String createdHtml() {
        return "<td><a href=\"" + this.url + "\">" + this.caption + "</a></td>\n";
    }
}

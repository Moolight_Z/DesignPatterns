package com.moonligt.designpatterns.designpatterns.factorymethod;

/**
 * @ClassName Product
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:32
 * @Version V1.0
 **/
public abstract class Product {
	public abstract void use();
}

package com.moonligt.designpatterns.designpatterns.abstractfactory;

/**
 * @ClassName AbstractFactory
 * @Description: 抽象工厂
 * @Author Moonlight
 * @Date 2020/5/16 13:35
 * @Version V1.0
 **/
public abstract class AbstractFactory {
    public static AbstractFactory getInstance(String className){
        AbstractFactory factory = null;
        try {
            factory = (AbstractFactory) Class.forName(className).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return factory;
    }

    public abstract Link createdLink(String name, String url);

    public abstract Tray createdTray(String caption);

    public abstract Page createdPage(String title, String author);

}

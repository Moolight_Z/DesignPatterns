package com.moonligt.designpatterns.designpatterns.abstractfactory.listfactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.Link;

/**
 * @ClassName ListLink
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 13:59
 * @Version V1.0
 **/
public class ListLink extends Link {

    public ListLink(String caption, String url) {
        super(caption, url);
    }

    @Override
    public String createdHtml() {
        return "<li><a href=\"" + this.url + "\">" + this.caption + "</a></li>";
    }
}

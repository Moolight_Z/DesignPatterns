package com.moonligt.designpatterns.designpatterns.abstractfactory.tablefactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.Item;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Tray;

/**
 * @ClassName TableTray
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:08
 * @Version V1.0
 **/
public class TableTray extends Tray {

    public TableTray(String caption) {
        super(caption);
    }

    @Override
    public String createdHtml() {
        StringBuilder s = new StringBuilder();

        s.append("<td>").append("<table><tr>").append("<td>").append(this.caption).append("</td>").append("</tr><tr>");

        for (Item item : tray) {
            s.append(item.createdHtml());
        }

        s.append("</tr></table></td>");

        return s.toString();
    }
}

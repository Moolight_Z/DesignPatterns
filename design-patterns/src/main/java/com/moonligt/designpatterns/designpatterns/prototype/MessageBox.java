package com.moonligt.designpatterns.designpatterns.prototype;

/**
 * @ClassName MessageBox
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 12:45
 * @Version V1.0
 **/
public class MessageBox implements Product  {
    private String msg;

    public MessageBox(String msg){
        this.msg = msg;
    }

    @Override
    public void use() {
        System.out.println("**********");
        System.out.println("*    " + msg + "   *");
        System.out.println("**********");
    }

    @Override
    public Product createClone() {
        try {
            return (Product) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

}

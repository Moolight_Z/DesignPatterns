package com.moonligt.designpatterns.designpatterns.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Directory
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 20:18
 * @Version V1.0
 **/
public class Directory extends Entry {

    private String name;
    private List<Entry> directory;

    public Directory(String name){
        this.name = name;
        this.directory = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getSize() {
        int size = 0;
        for (Entry entry : this.directory) {
            size += entry.getSize();
        }
        return size;
    }

    @Override
    public void print() {
        System.out.println(this.name);
        for (Entry entry : this.directory) {
            entry.print();
        }
    }

    public void add(Entry entry) {
        this.directory.add(entry);
    }
}

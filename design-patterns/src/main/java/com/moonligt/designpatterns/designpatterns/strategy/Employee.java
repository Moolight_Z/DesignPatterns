package com.moonligt.designpatterns.designpatterns.strategy;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 * @author Moonlight
 * @date 2020/5/6 13:22
 */
@Data
public class Employee {
    private Integer age;
    private Integer identificationNumber;
    private String name;

    public Employee(Integer age, Integer identificationNumber, String name){
        this.age = age;
        this.identificationNumber = identificationNumber;
        this.name = name;
    }

    public static void main(String[] args) {
        List<Employee> list = new ArrayList<>();
        list.add(new Employee(2, 2, "num - 2"));
        list.add(new Employee(1, 11, "num - 1"));
        list.add(new Employee(10, 10, "num - 10"));
        list.add(new Employee(8, 8, "num - 8"));
        list.add(new Employee(1, 1, "num - 1_2"));
        for (Employee employee : list) {
            System.out.println(employee.toString());
        }

        SortStrategy sortStrategy = new EmployeeAgeSortStrategy();
        System.out.println("                    ");
        sortStrategy.sort(list);
        for (Employee employee : list) {
            System.out.println(employee.toString());
        }

        sortStrategy = new EmployeeIdNumberSortStrategy();
        System.out.println("                    ");
        sortStrategy.sort(list);
        for (Employee employee : list) {
            System.out.println(employee.toString());
        }
    }
}

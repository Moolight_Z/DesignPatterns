package com.moonligt.designpatterns.designpatterns.chainofresponsibility.pushoff;

import lombok.Data;

/**
 * @ClassName Support
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:35
 * @Version V1.0
 **/
@Data
public abstract class Support {
    private String name;
    private Support next;

    public Support(String name){
        this.name = name;
    }

    public Support setNext(Support next){
        this.next = next;
        return this.next;
    }

    public final void support(Trouble trouble){
        if (resolve(trouble)) {
            done(trouble);
        } else if (this.next != null) {
            this.next.support(trouble);
        } else {
            fail(trouble);
        }
    }

    public void fail(Trouble trouble){
        System.out.println(this.name + "解决不了" + trouble.getNumber());
    };

    public void done(Trouble trouble){
        System.out.println(trouble.getNumber() + "已经被" + this.name + "解决");
    };

    protected abstract boolean resolve(Trouble trouble);


}

package com.moonligt.designpatterns.designpatterns.abstractfactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Tray
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 13:56
 * @Version V1.0
 **/
public abstract class Tray extends Item {

    protected List<Item> tray;

    public Tray(String caption) {
        super(caption);
        this.tray = new ArrayList<>();
    }

    public void addItem(Item item){
        tray.add(item);
    }
}

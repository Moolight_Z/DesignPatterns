package com.moonligt.designpatterns.designpatterns.state;

/**
 * @ClassName LiftState
 * @Description: 状态。
 * @Author Moonlight
 * @Date 2020/5/16 20:59
 * @Version V1.0
 **/
public abstract class LiftState {

    protected Context context;

    public void setContext(Context context){
        this.context = context;
    }

    public abstract void open();
    public abstract void close();
    public abstract void run();
    public abstract void stop();
}

package com.moonligt.designpatterns.designpatterns.adapter.extend;

import com.moonligt.designpatterns.designpatterns.adapter.Banner;
import com.moonligt.designpatterns.designpatterns.adapter.Print;

/**
 * @ClassName PrintBanner
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:21
 * @Version V1.0
 **/
public class PrintBanner extends Banner implements Print {
    @Override
    public void printWeak(String str) {
        System.out.println(super.showWithParen(str));
    }

    @Override
    public void printStrong(String str) {
        System.out.println(super.showWithAster(str));
    }
}

package com.moonligt.designpatterns.designpatterns.factorymethod;

/**
 * @ClassName TVFactory
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:35
 * @Version V1.0
 **/
public class TVFactory extends Factory {

	@Override
	public Product createProduct(String owner) {
		return new TV(owner);
	}

}

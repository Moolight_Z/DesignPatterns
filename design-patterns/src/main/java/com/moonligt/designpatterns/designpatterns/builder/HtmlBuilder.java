package com.moonligt.designpatterns.designpatterns.builder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @ClassName HtmlBuilder
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 11:42
 * @Version V1.0
 **/
public class HtmlBuilder extends Builder {

	private String fileName;
	private PrintWriter pw;
	
	@Override
	public void makeTitle(String title) {
		fileName = title + ".html";
		try{
			pw = new PrintWriter(new FileWriter(fileName));
		}catch(RuntimeException | IOException e){
			e.printStackTrace();
		}
		pw.write("<html><head><title>" + title + "</title></head><body>");
		pw.write("<h1>" + title + "</h1>");
	}

	@Override
	public void makeContent(String content) {
		pw.write("<p>" + content + "</p>");
	}

	@Override
	public void makeItems(String[] items) {
		pw.write("<ul>");
		for (String item : items) {
			pw.write("<li>" + item + "</li>");
		}
		pw.write("</ul>");
	}

	@Override
	public void close() {
		pw.write("</body></html>");
		pw.flush();
		pw.close();
	}
	
	public String getResult(){
		return fileName;
	}
	
	public static void main(String[] args){
		Director text = new Director(new TextBuilder());
		text.construct();
		Director html = new Director(new HtmlBuilder());
		html.construct();
	}

}

package com.moonligt.designpatterns.designpatterns.composite;

/**
 * @ClassName File
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 20:17
 * @Version V1.0
 **/
public class File extends Entry {

    private String name;
    private int size;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public void print() {
        System.out.println(this.name);
    }
}

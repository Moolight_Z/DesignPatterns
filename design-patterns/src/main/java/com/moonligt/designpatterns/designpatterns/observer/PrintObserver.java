package com.moonligt.designpatterns.designpatterns.observer;

/**
 * @ClassName PrintObserver
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 18:49
 * @Version V1.0
 **/
public class PrintObserver implements Observer {
    @Override
    public void onEvent(Event event) {
        System.out.println("printObserver: " + event.getEvent());
    }
}

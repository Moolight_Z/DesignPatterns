package com.moonligt.designpatterns.designpatterns.future;

import lombok.ToString;

/**
 * @ClassName FutureData
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/7/18 23:32
 * @Version V1.0
 **/
@ToString
public class FutureData implements Data {
    private RealData realData;
    private boolean isReady = false;

    @Override
    public String getResult() {
        while (!isReady) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return this.realData.getResult();
    }

    public synchronized void setRealData(RealData realData){
        if (this.isReady) {
            return;
        }
        this.realData = realData;
        this.isReady = true;
        notifyAll();
    }

}

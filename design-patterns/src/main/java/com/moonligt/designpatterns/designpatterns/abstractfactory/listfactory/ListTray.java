package com.moonligt.designpatterns.designpatterns.abstractfactory.listfactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.Item;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Tray;

/**
 * @ClassName ListTray
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:00
 * @Version V1.0
 **/
public class ListTray extends Tray {

    public ListTray(String caption) {
        super(caption);
    }

    @Override
    public String createdHtml() {
        StringBuilder sb = new StringBuilder();
        sb.append("<li>\n").append(caption).append("\n").append("<ul>\n");
        for(Item item : tray){
            sb.append(item.createdHtml());
        }
        sb.append("</ul>\n").append("</li>\n");
        return sb.toString();
    }
}

package com.moonligt.designpatterns.designpatterns.chainofresponsibility.pushoff;

/**
 * @ClassName SpecialSupport
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:44
 * @Version V1.0
 **/
public class SpecialSupport extends Support {
    private int specialNum;

    public SpecialSupport(String name, int specialNum){
        super(name);
        this.specialNum = specialNum;
    }

    @Override
    protected boolean resolve(Trouble trouble) {
        return trouble.getNumber() == specialNum;
    }
}

package com.moonligt.designpatterns.designpatterns.future;

/**
 * @ClassName Data
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/7/18 23:30
 * @Version V1.0
 **/
public interface Data {
    String getResult();
}

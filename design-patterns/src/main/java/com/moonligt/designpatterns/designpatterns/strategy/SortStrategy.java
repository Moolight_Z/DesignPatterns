package com.moonligt.designpatterns.designpatterns.strategy;

import java.util.Collections;
import java.util.List;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 * @author Moonlight
 * @date 2020/5/6 13:26
 */
public interface SortStrategy {

    int[] partition(Employee[] arr, int start, int end);

    default void sort(List<Employee> employeeList) {
        Employee[] arr = new Employee[employeeList.size()];
        employeeList.toArray(arr);
        sort(arr, 0, arr.length - 1);
        employeeList.clear();
        Collections.addAll(employeeList, arr);
    }

    default void sort(Employee[] arr, int start, int end) {
        if (start >= end) {
            return;
        }
        int[] res = partition(arr, start, end);
        sort(arr, start, res[0] - 1);
        sort(arr, res[1] + 1, end);
    }

    default void swap(Employee[] arr, int left, int right) {
        Employee temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
    }
}

package com.moonligt.designpatterns.designpatterns.decorator;

/**
 * @ClassName SideBorder
 * @Description: 实际装饰物
 * @Author Moonlight
 * @Date 2020/5/16 20:35
 * @Version V1.0
 **/
public class SideBorder extends Border{
    private char border;

    public SideBorder(Display display, char border) {
        super(display);
        this.border = border;
    }

    @Override
    public int getColumns() {
        return 2 + this.display.getColumns();
    }

    @Override
    public int getRows() {
        return display.getRows();
    }

    @Override
    public String getRowText(int row) {
        return this.border + display.getRowText(row) + this.border;
    }
}

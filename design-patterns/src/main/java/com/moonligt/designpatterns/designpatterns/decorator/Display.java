package com.moonligt.designpatterns.designpatterns.decorator;

/**
 * @ClassName Display
 * @Description: 被装饰物
 * @Author Moonlight
 * @Date 2020/5/16 20:28
 * @Version V1.0
 **/
public abstract class Display {

    public abstract int getColumns();

    public abstract int getRows();

    public abstract String getRowText(int row);

    public final void show () {
        for (int i = 0; i < getRows(); i++) {
            System.out.println(getRowText(i));
        }
    }
}

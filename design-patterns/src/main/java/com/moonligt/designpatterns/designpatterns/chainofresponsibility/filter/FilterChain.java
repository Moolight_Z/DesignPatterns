package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FilterChain
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 15:51
 * @Version V1.0
 **/
public class FilterChain implements Filter {
    private List<Filter> filterList;
    private int index;

    public FilterChain(){
        this.filterList = new ArrayList<>();
        this.index = 0;
    }

    public FilterChain addFilter(Filter filter) {
        this.filterList.add(filter);
        return this;
    }

    @Override
    public boolean doFilter(Req req, Resp resp, FilterChain chain) {
        if (index == filterList.size()) {
            return false;
        }
        Filter f = filterList.get(index);
        index++;
        return f.doFilter(req, resp, chain);
    }
}

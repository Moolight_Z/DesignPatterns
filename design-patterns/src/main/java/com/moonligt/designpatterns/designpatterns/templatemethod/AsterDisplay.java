package com.moonligt.designpatterns.designpatterns.templatemethod;

/**
 * @ClassName AsterDisplay
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:42
 * @Version V1.0
 **/
public class AsterDisplay extends AbstractDisplay  {

    public AsterDisplay(String str){
        setStr(str);
    }

    @Override
    public void begin() {
        printAster();
    }

    @Override
    public void print() {
        System.out.println("*" + getStr() + "*");
    }

    @Override
    public void end() {
        printAster();
    }

    private void printAster(){
        for (int i = 0, length = getStr().length(); i < length; i++) {
            System.out.print("*");
        }
        System.out.println("*");
    }
}

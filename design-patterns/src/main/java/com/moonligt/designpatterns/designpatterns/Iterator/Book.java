package com.moonligt.designpatterns.designpatterns.Iterator;

import java.util.Iterator;

/**
 * @ClassName Book
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 10:30
 * @Version V1.0
 **/
public class Book  {

	private String name;
	
	public String getName(){
		return this.name;
	}
	
	public Book(String name){this.name = name;}
}

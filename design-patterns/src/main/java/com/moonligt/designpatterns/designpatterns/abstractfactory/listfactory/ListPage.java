package com.moonligt.designpatterns.designpatterns.abstractfactory.listfactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.Item;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Page;

/**
 * @ClassName ListPage
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:03
 * @Version V1.0
 **/
public class ListPage extends Page {

    public ListPage(String title, String author) {
        super(title, author);
    }

    @Override
    public String makeHtml() {
        StringBuilder s = new StringBuilder();
        s.append("<html><head><title>").append(this.title).append("</title></head>\n");
        s.append("<body>\n");
        s.append("<h1>").append(this.title).append("</h1>\n");
        s.append("<ul>\n");
        for(Item item : this.content){
            s.append(item.createdHtml());
        }
        s.append("</ul>\n");
        s.append("<hr><address>").append(this.author).append("</address></hr>");
        s.append("</body>\n</html>");
        return s.toString();
    }
}

package com.moonligt.designpatterns.designpatterns.chainofresponsibility.pushoff;

/**
 * @ClassName Test
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:46
 * @Version V1.0
 **/
public class Test {

    public static void main(String[] args){
        Support a = new NoSupport("A");
        Support b = new OddSupport("B");
        Support c = new SpecialSupport("C", 4);

        a.setNext(b).setNext(c);

        for (int i = 0; i < 6; i++) {
            a.support(new Trouble(i));
        }
    }

}

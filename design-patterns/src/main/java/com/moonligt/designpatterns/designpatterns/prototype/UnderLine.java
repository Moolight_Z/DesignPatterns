package com.moonligt.designpatterns.designpatterns.prototype;

/**
 * @ClassName UnderLine
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 12:46
 * @Version V1.0
 **/
public class UnderLine implements Product {
    private String msg;

    public UnderLine(String msg){
        this.msg = msg;
    }

    @Override
    public void use() {
        System.out.println(msg);
        System.out.println("____________");
    }

    @Override
    public Product createClone() {
        try {
            return (Product) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}

package com.moonligt.designpatterns.designpatterns.future;

import java.util.concurrent.*;

/**
 * @ClassName Test
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/7/18 23:33
 * @Version V1.0
 **/
public class Test {

    public Data testFuture(final String params) {
        final FutureData futureData = new FutureData();
        new Thread(() -> {
            RealData realData = new RealData(params);
            futureData.setRealData(realData);
        }).start();
        return futureData;
    }

    public static void main(String[] args) {
        Test test = new Test();

        Data data = test.testFuture("123,");

        System.out.println(data);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(data);

        System.out.println("===================================================================");

        FutureTask<String> task = new FutureTask<>(new RealData("123,"));

        ExecutorService service = Executors.newFixedThreadPool(1);
        service.submit(task);
        try {
            System.out.println(task.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        service.shutdown();
    }

}

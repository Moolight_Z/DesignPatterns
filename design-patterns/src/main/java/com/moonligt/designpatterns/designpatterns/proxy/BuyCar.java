package com.moonligt.designpatterns.designpatterns.proxy;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 * @author Moonlight
 * @date 2020/5/13 16:18
 */
public class BuyCar implements Buy {
    @Override
    public void buyCar() {
        System.out.println("买一辆车");
    }
}

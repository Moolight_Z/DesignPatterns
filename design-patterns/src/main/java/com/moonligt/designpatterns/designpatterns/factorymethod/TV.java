package com.moonligt.designpatterns.designpatterns.factorymethod;

/**
 * @ClassName TV
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:33
 * @Version V1.0
 **/
public class TV extends Product {

	private String owner;
	
	public TV(String owner){
		System.out.println(owner + " buy TV");
		this.owner = owner;
	}
	
	@Override
	public void use() {
		System.out.println(owner + " watch TV show ");
	}

}

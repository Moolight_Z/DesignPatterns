package com.moonligt.designpatterns.designpatterns.bridge;

/**
 * @ClassName Display
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 15:00
 * @Version V1.0
 **/
public class Display {
    private AbstractDisplayImpl impl;

    public Display(AbstractDisplayImpl impl) {
        this.impl = impl;
    }

    public void open(){
        this.impl.rawOpen();
    }

    public void print(){
        this.impl.rawPrint();
    }

    public void close(){
        this.impl.rawClose();
    }

    public final void display(){
        this.open();
        this.print();
        this.close();
    }

    public static void main(String[] args){
        Display d1 = new Display(new StringDisplay("hello world 11111"));
        Display d2 = new CountDisplay(new StringDisplay("hello world 22222"));
        CountDisplay d3 = new CountDisplay(new StringDisplay("hello world 3333"));

        d1.display();

        d2.display();

        d3.display();
        d3.multiDisplay(3);
    }

}

package com.moonligt.designpatterns.designpatterns.factorymethod;

/**
 * @ClassName Factory
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:30
 * @Version V1.0
 **/
public abstract class Factory {
	public abstract Product createProduct(String owner) ;
}

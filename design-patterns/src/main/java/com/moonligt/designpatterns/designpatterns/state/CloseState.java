package com.moonligt.designpatterns.designpatterns.state;

/**
 * @ClassName CloseState
 * @Description: 具体状态
 * @Author Moonlight
 * @Date 2020/5/16 21:06
 * @Version V1.0
 **/
public class CloseState extends LiftState {
    @Override
    public void open() {
        super.context.setState(Context.OPEN_STATE);
        super.context.getState().open();
    }

    @Override
    public void close() {
        System.out.println("电梯关门...");
    }

    @Override
    public void run() {
        super.context.setState(Context.RUN_STATE);
        super.context.getState().run();
    }

    @Override
    public void stop() {
        super.context.setState(Context.STOP_STATE);
        super.context.getState().stop();
    }
}

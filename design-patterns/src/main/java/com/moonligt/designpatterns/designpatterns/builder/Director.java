package com.moonligt.designpatterns.designpatterns.builder;

/**
 * @ClassName Director
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 11:40
 * @Version V1.0
 **/
public class Director {
	
	private Builder builder;
	
	public Director(Builder builder) {
		this.builder = builder;
	}
	
	public void construct() {
		builder.makeTitle("TITLE");
		builder.makeContent(" .. Content .. ");
		builder.makeItems(new String[]{
				"123",
				"456",
				"789",
				"1011"
		});
		builder.close();
		System.out.println(builder.getResult());
	}

}

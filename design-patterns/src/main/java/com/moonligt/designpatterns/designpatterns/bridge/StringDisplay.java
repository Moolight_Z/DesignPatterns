package com.moonligt.designpatterns.designpatterns.bridge;

/**
 * @ClassName StringDisplay
 * @Description: 实现层次
 * @Author Moonlight
 * @Date 2020/5/16 15:05
 * @Version V1.0
 **/
public class StringDisplay extends AbstractDisplayImpl {

    private String string;
    private int width;

    public StringDisplay(String string){
        this.string = string;
        this.width = string.length();
    }

    @Override
    public void rawOpen() {
        this.printLine();
    }

    @Override
    public void rawPrint() {
        System.out.println("| " + this.string + " |");
    }

    @Override
    public void rawClose() {
        this.printLine();
    }

    private void printLine(){
        System.out.print("+");
        for (int i = 0; i < this.width; i++) {
            System.out.print("-");
        }
        System.out.println("+");
    }

}

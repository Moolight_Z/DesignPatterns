package com.moonligt.designpatterns.designpatterns.bridge;

/**
 * @ClassName DisplayImpl
 * @Description: 将类的功能层次结构和实现层次结构分离开，想要增加功能时，只需要在功能层次结构新增，不必对实现层次进行修改
 * @Author Moonlight
 * @Date 2020/5/16 15:04
 * @Version V1.0
 **/
public abstract class AbstractDisplayImpl {
    public abstract void rawOpen();

    public abstract void rawPrint();

    public abstract void rawClose();
}
package com.moonligt.designpatterns.designpatterns.state;

/**
 * @ClassName RunState
 * @Description: 具体状态
 * @Author Moonlight
 * @Date 2020/5/16 21:06
 * @Version V1.0
 **/
public class RunState extends LiftState {
    @Override
    public void open() {
    }

    @Override
    public void close() {
    }

    @Override
    public void run() {
        System.out.println("电梯启动...");
    }

    @Override
    public void stop() {
        super.context.setState(Context.STOP_STATE);
        super.context.getState().stop();
    }
}

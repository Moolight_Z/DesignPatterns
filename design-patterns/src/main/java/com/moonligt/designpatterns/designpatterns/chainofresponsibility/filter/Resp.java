package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

import lombok.Data;

/**
 * @ClassName Resp
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 15:50
 * @Version V1.0
 **/
@Data
public class Resp {
    private String respMsg;
    public Resp(String respMsg){this.respMsg = respMsg;}
}

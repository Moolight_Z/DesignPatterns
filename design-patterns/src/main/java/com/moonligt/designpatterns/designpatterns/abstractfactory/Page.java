package com.moonligt.designpatterns.designpatterns.abstractfactory;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Page
 * @Description: 抽象产品
 * @Author Moonlight
 * @Date 2020/5/16 13:45
 * @Version V1.0
 **/
public abstract class Page {
    protected String title, author;
    protected List<Item> content;

    public Page(String title, String author) {
        this.title = title;
        this.author = author;
        this.content = new ArrayList<>();
    }

    public abstract String makeHtml();

    public void addItem(Item item){
        this.content.add(item);
    }

    public void output(){
        String filename = this.title + ".html";
        Writer writer = null;
        try {
            writer = new FileWriter(filename);
            writer.write(this.makeHtml());
            System.out.println(filename + "编写完毕");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.flush();
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

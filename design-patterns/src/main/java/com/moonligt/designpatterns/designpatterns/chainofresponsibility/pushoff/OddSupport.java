package com.moonligt.designpatterns.designpatterns.chainofresponsibility.pushoff;

/**
 * @ClassName OddSupport
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:42
 * @Version V1.0
 **/
public class OddSupport extends Support {
    public OddSupport (String name) {
        super(name);
    }
    @Override
    protected boolean resolve(Trouble trouble) {
        return (trouble.getNumber() & 1) == 1;
    }
}

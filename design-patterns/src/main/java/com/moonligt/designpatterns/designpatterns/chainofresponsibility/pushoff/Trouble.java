package com.moonligt.designpatterns.designpatterns.chainofresponsibility.pushoff;

import lombok.Data;

/**
 * @ClassName Trouble
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:36
 * @Version V1.0
 **/
@Data
public class Trouble {

    private int number;

    public Trouble(int number){this.number = number;}

}

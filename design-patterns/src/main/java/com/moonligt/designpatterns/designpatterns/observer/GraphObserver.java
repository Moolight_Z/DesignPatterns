package com.moonligt.designpatterns.designpatterns.observer;

/**
 * @ClassName GraphObserver
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 18:51
 * @Version V1.0
 **/
public class GraphObserver implements Observer {
    @Override
    public void onEvent(Event event) {
        StringBuilder stringBuilder = new StringBuilder("GraphObserver: ");
        for (int i = 0; i < event.getEvent(); i++) {
            stringBuilder.append("*");
        }
        System.out.println(stringBuilder.toString());
    }
}

package com.moonligt.designpatterns.designpatterns.abstractfactory.listfactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.AbstractFactory;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Link;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Page;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Tray;

/**
 * @ClassName ListFactory
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 13:58
 * @Version V1.0
 **/
public class ListFactory extends AbstractFactory {
    @Override
    public Link createdLink(String name, String url) {
        return new ListLink(name, url);
    }

    @Override
    public Tray createdTray(String caption) {
        return null;
    }

    @Override
    public Page createdPage(String title, String author) {
        return null;
    }
}

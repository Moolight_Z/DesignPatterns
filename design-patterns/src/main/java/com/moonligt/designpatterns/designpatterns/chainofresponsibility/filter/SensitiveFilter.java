package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

/**
 * @ClassName SensitiveFilter
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:02
 * @Version V1.0
 **/
public class SensitiveFilter implements Filter {
    @Override
    public boolean doFilter(Req req, Resp resp, FilterChain chain) {
        req.setReqMsg(req.getReqMsg() + " - sensitiveFilter");
        chain.doFilter(req, resp, chain);
        resp.setRespMsg(resp.getRespMsg() + " - sensitiveFilter");
        return req.getReqMsg().contains("abc");
    }
}

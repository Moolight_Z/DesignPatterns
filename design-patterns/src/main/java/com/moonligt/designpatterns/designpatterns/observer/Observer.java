package com.moonligt.designpatterns.designpatterns.observer;

/**
 * @ClassName Observer
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 17:33
 * @Version V1.0
 **/
public interface Observer {
    void onEvent(Event event);
}

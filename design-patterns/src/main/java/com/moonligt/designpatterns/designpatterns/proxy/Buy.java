package com.moonligt.designpatterns.designpatterns.proxy;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 * @author Moonlight
 * @date 2020/5/13 16:17
 */
public interface Buy {

    void buyCar();

}

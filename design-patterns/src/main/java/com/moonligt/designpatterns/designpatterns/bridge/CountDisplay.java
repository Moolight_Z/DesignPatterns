package com.moonligt.designpatterns.designpatterns.bridge;

/**
 * @ClassName CountDisplay
 * @Description: 功能层次
 * @Author Moonlight
 * @Date 2020/5/16 15:10
 * @Version V1.0
 **/
public class CountDisplay extends Display {
    public CountDisplay(AbstractDisplayImpl impl) {
        super(impl);
    }

    public void multiDisplay(int times){
        super.open();
        for (int i = 0; i < times; i++) {
            super.print();
        }
        super.close();
    }
}

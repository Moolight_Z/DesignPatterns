package com.moonligt.designpatterns.designpatterns.state;

/**
 * @ClassName OpenState
 * @Description: 具体状态
 * @Author Moonlight
 * @Date 2020/5/16 21:02
 * @Version V1.0
 **/
public class OpenState extends LiftState {
    @Override
    public void open() {
        System.out.println("电梯开门...");
    }

    @Override
    public void close() {
        super.context.setState(Context.CLOSE_STATE);
        super.context.getState().close();
    }

    @Override
    public void run() {
    }

    @Override
    public void stop() {
    }
}

package com.moonligt.designpatterns.designpatterns.abstractfactory;

/**
 * @ClassName Link
 * @Description: 抽象零件
 * @Author Moonlight
 * @Date 2020/5/16 13:40
 * @Version V1.0
 **/
public abstract class Link extends Item {

    protected String url;

    public Link(String caption, String url) {
        super(caption);
        this.url = url;
    }
}

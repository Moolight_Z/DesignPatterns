package com.moonligt.designpatterns.designpatterns.Iterator;

import java.util.Iterator;

/**
 * @ClassName Aggregate
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 10:28
 * @Version V1.0
 **/
public interface Aggregate {
    default Iterator iterator() {
        return null;
    }
}

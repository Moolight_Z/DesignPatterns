package com.moonligt.designpatterns.designpatterns.templatemethod;

/**
 * @ClassName ParenDisplay
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:43
 * @Version V1.0
 **/
public class ParenDisplay extends AbstractDisplay {

    public ParenDisplay(String str){
        this.setStr(str);
    }

    @Override
    public void begin() {
        System.out.print("(");
    }

    @Override
    public void print() {
        System.out.print(getStr());
    }

    @Override
    public void end() {
        System.out.print(")");
    }
}

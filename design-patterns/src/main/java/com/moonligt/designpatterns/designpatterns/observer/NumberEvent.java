package com.moonligt.designpatterns.designpatterns.observer;

import lombok.Data;

/**
 * @ClassName NumberEvent
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 18:22
 * @Version V1.0
 **/
@Data
public class NumberEvent extends Event {
    private int num;

    @Override
    public int getEvent() {
        return this.num;
    }

    @Override
    public void push() {
        for (int i = 1; i < 11; i++) {
            this.num = i;
            notifyObserver();
        }
    }
}

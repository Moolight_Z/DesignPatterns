package com.moonligt.designpatterns.designpatterns.builder;

/**
 * @ClassName Builder
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 11:38
 * @Version V1.0
 **/
public abstract class Builder {
	public abstract void makeTitle(String title);
	public abstract void makeContent(String content);
	public abstract void makeItems(String[] items);
	public abstract void close();
	public abstract String getResult();
}

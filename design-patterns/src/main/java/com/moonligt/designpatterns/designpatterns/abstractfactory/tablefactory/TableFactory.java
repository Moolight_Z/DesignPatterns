package com.moonligt.designpatterns.designpatterns.abstractfactory.tablefactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.AbstractFactory;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Link;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Page;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Tray;

/**
 * @ClassName TableFactory
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:06
 * @Version V1.0
 **/
public class TableFactory extends AbstractFactory {
    @Override
    public Link createdLink(String name, String url) {
        return null;
    }

    @Override
    public Tray createdTray(String caption) {
        return null;
    }

    @Override
    public Page createdPage(String title, String author) {
        return null;
    }
}

package com.moonligt.designpatterns.designpatterns.future;

import lombok.ToString;

import java.util.concurrent.Callable;

/**
 * @ClassName RealData
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/7/18 23:30
 * @Version V1.0
 **/
@ToString
public class RealData implements Data, Callable<String> {
    private final String result;
    private String param;

    public RealData(String params) {
        this.result = "abc";
        this.param = params;
    }

    @Override
    public String getResult() {
        return this.result;
    }

    @Override
    public String call() throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            stringBuilder.append(param);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return stringBuilder.toString();
    }
}

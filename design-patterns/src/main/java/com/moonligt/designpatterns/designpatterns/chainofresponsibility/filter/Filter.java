package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

/**
 * @ClassName Filter
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 15:50
 * @Version V1.0
 **/
public interface Filter {
    boolean doFilter(Req req, Resp resp, FilterChain chain);
}

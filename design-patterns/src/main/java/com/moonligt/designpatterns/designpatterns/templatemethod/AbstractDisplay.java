package com.moonligt.designpatterns.designpatterns.templatemethod;

import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName AbstractDisplay
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:40
 * @Version V1.0
 **/
public abstract class AbstractDisplay {
    @Getter
    @Setter
    protected String str;

    public abstract void begin ();
    public abstract void print ();
    public abstract void end ();

    public final void display () {
        begin();
        for (int i = 0; i < 5; i++) {
            print();
        }
        end();
    }
}

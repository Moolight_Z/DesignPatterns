package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

/**
 * @ClassName Test
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:16
 * @Version V1.0
 **/
public class Test {

    public static void main (String[] args){
        Req req = new Req("<script>https://123iasdnia</script>hhhhh");
        Resp resp = new Resp("this is resp");

        FilterChain chain = new FilterChain();
        chain.addFilter(new ScriptFilter()).addFilter(new SensitiveFilter()).addFilter(new UrlFilter());

        chain.doFilter(req, resp, chain);

        System.out.println(req.getReqMsg());
        System.out.println(resp.getRespMsg());
    }

}

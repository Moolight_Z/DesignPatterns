package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

import lombok.Data;

/**
 * @ClassName Req
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 15:49
 * @Version V1.0
 **/
@Data
public class Req {
    private String reqMsg;
    public Req(String reqMsg){this.reqMsg = reqMsg;}
}

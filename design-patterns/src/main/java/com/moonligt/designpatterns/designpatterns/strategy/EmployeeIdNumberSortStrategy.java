package com.moonligt.designpatterns.designpatterns.strategy;

import java.util.Collections;
import java.util.List;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 * @author Moonlight
 * @date 2020/5/6 13:29
 */
public class EmployeeIdNumberSortStrategy implements SortStrategy {
    @Override
    public int[] partition(Employee[] arr, int start, int end) {
        Employee pivot = arr[end];
        int left = start, right = end - 1;

        while (left <= right) {
            while (left <= right && arr[left].getIdentificationNumber() <= pivot.getIdentificationNumber()) {
                left++;
            }
            while (left <= right && arr[right].getIdentificationNumber() > pivot.getIdentificationNumber()) {
                right--;
            }
            if (left <= right) {
                swap(arr, left, right);
            }
        }
        swap(arr, left, end);
        return new int[]{left, right};
    }
}

package com.moonligt.designpatterns.designpatterns.decorator;

/**
 * @ClassName Main
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 20:37
 * @Version V1.0
 **/
public class Main {

    public static void main(String[] args){
        Display d1 = new StringDisplay("hello world");
        Display d2 = new SideBorder(d1, '!');
        d1.show();
        d2.show();
        Display d3 = new SideBorder(d2, '$');
        d3.show();
    }

}

package com.moonligt.designpatterns.designpatterns.decorator;

/**
 * @ClassName Border
 * @Description: 装饰物
 * @Author Moonlight
 * @Date 2020/5/16 20:33
 * @Version V1.0
 **/
public abstract class Border extends Display {
    protected Display display;

    public Border(Display display){
        this.display = display;
    }

}

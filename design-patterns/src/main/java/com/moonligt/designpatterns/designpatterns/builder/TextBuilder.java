package com.moonligt.designpatterns.designpatterns.builder;

/**
 * @ClassName TextBuilder
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 11:45
 * @Version V1.0
 **/
public class TextBuilder extends Builder {
	private StringBuilder sb = new StringBuilder();
	@Override
	public void makeTitle(String title) {
		sb.append("[").append(title).append("]\n");
	}

	@Override
	public void makeContent(String content) {
		sb.append(content).append("\n");
	}

	@Override
	public void makeItems(String[] items) {
		for(int i = 0, length = items.length; i < length; i++){
			sb.append(i + 1).append(".").append(items[i]).append("\n");
		}
	}

	@Override
	public void close() {
		sb.append("\n");
	}
	
	public String getResult() {
		return sb.toString();
	}

}

package com.moonligt.designpatterns.designpatterns.chainofresponsibility.pushoff;

/**
 * @ClassName NoSupport
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 16:41
 * @Version V1.0
 **/
public class NoSupport extends Support {
    public NoSupport(String name) {
        super(name);
    }

    @Override
    protected boolean resolve(Trouble trouble) {
        return false;
    }
}

package com.moonligt.designpatterns.designpatterns.adapter.entrust;

import com.moonligt.designpatterns.designpatterns.adapter.Banner;
import com.moonligt.designpatterns.designpatterns.adapter.Print;

/**
 * @ClassName PrintBanner
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:25
 * @Version V1.0
 **/
public class PrintBanner implements Print {

    private Banner banner;

    public PrintBanner(Banner banner){
        this.banner = banner;
    }

    @Override
    public void printWeak(String str) {
        System.out.println(banner.showWithParen(str));
    }

    @Override
    public void printStrong(String str) {
        System.out.println(banner.showWithAster(str));
    }
}

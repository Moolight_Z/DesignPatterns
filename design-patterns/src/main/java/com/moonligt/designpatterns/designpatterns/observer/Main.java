package com.moonligt.designpatterns.designpatterns.observer;

/**
 * @ClassName Main
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 18:53
 * @Version V1.0
 **/
public class Main {

    public static void main(String[] args){
        Event event = new NumberEvent();
        Observer print = new PrintObserver(), graph = new GraphObserver();
        event.addObserver(print).addObserver(graph);
        event.push();
    }

}

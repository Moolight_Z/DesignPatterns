package com.moonligt.designpatterns.designpatterns.prototype;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Manager
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 12:44
 * @Version V1.0
 **/
public class Manager {
    private Map<String, Product> showcase = new HashMap();

    public void register(String name, Product proto) {
        showcase.put(name, proto);
    }

    public Product created(String name) {
        return (Product)showcase.get(name) != null ? (Product)showcase.get(name).createClone() : null;
    }
}

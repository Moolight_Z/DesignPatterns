package com.moonligt.designpatterns.designpatterns.adapter;

/**
 * @ClassName Print
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:21
 * @Version V1.0
 **/
public interface Print {
    void printWeak(String str);

    void printStrong(String str);
}

package com.moonligt.designpatterns.designpatterns.abstractfactory.tablefactory;

import com.moonligt.designpatterns.designpatterns.abstractfactory.Item;
import com.moonligt.designpatterns.designpatterns.abstractfactory.Page;

/**
 * @ClassName TablePage
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:13
 * @Version V1.0
 **/
public class TablePage extends Page {

    public TablePage(String title, String author) {
        super(title, author);
    }

    @Override
    public String makeHtml() {
        StringBuilder s = new StringBuilder();

        s.append("<html><head><title>").append(this.title).append("</title></head>\n");
        s.append("<body>\n");
        s.append("<h1>").append(this.title).append("</h1>\n");
        s.append("<table>");

        for (Item item : this.content) {
            s.append("<tr>").append(item.createdHtml()).append("</tr>");
        }

        s.append("</table>\n");

        s.append("<hr>").append(this.author).append("</hr>").append("</body></html>");

        return s.toString();
    }
}

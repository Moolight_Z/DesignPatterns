package com.moonligt.designpatterns.designpatterns.composite;

/**
 * @ClassName Entry
 * @Description: 多用于树状结构
 * @Author Moonlight
 * @Date 2020/5/16 20:16
 * @Version V1.0
 **/
public abstract class Entry {
    public abstract String getName();
    public abstract int getSize();
    public abstract void print();
    public void add(Entry entry){
        throw new RuntimeException();
    };
}

package com.moonligt.designpatterns.designpatterns.strategy;

import java.util.Collections;
import java.util.List;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 * @author Moonlight
 * @date 2020/5/6 13:27
 */
public class EmployeeAgeSortStrategy implements SortStrategy {
    @Override
    public int[] partition(Employee[] arr, int start, int end) {
        int left = start, right = end - 1;
        Employee pivot = arr[end];

        while (left <= right) {
            while (left <= right && arr[left].getAge() <= pivot.getAge()) {
                left++;
            }
            while (left <= right && arr[right].getAge() > pivot.getAge()) {
                right--;
            }
            if (left <= right) {
                swap(arr, left, right);
            }
        }
        swap(arr, left, end);
        return new int[]{left, right};
    }
}

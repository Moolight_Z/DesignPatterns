package com.moonligt.designpatterns.designpatterns.factorymethod;

/**
 * @ClassName IDCardFactory
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:35
 * @Version V1.0
 **/
public class IDCardFactory extends Factory {

	@Override
	public Product createProduct(String owner) {
		return new IDCard(owner);
	}
	
	public static void main (String[] args) {
		Factory f = new IDCardFactory();
		Product p = f.createProduct("moonlight");
		p.use();
		f = new TVFactory();
		p = f.createProduct("moonlight");
		p.use();
	}

}

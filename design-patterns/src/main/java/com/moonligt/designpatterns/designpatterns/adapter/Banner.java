package com.moonligt.designpatterns.designpatterns.adapter;

/**
 * @ClassName Banner
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 14:20
 * @Version V1.0
 **/
public class Banner {

    public String showWithParen(String str){
        return "(" + str + ")";
    }

    public String showWithAster(String str){
        return "**" + str + "**";
    }

}

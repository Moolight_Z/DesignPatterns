package com.moonligt.designpatterns.designpatterns.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Event
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 17:55
 * @Version V1.0
 **/
public abstract class Event {
    private List<Observer> observerList;

    public Event(){
        this.observerList = new ArrayList<>();
    }

    public Event addObserver(Observer observer){
        this.observerList.add(observer);
        return this;
    }

    public void removeObserver(Observer observer){
        this.observerList.remove(observer);
    }

    public void notifyObserver(){
        for (Observer observer : this.observerList) {
            observer.onEvent(this);
        }
    }

    public abstract int getEvent();

    public abstract void push();

}

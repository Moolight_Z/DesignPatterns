package com.moonligt.designpatterns.designpatterns.state;

/**
 * @ClassName Main
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 21:14
 * @Version V1.0
 **/
public class Main {
    public static void main (String[] args) {
        Context context = new Context();
        context.setState(new CloseState());

        context.open();
        context.close();
        context.run();
        context.stop();
    }
}

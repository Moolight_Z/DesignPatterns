package com.moonligt.designpatterns.designpatterns.abstractfactory;

/**
 * @ClassName Item
 * @Description: 抽象零件
 * @Author Moonlight
 * @Date 2020/5/16 13:39
 * @Version V1.0
 **/
public abstract class Item {

    protected String caption;

    public Item(String caption){
        this.caption = caption;
    }

    public abstract String createdHtml();
}

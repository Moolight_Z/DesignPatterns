package com.moonligt.designpatterns.designpatterns.proxy;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 * @author Moonlight
 * @date 2020/5/13 16:17
 */
public class BuyCarProxy implements Buy {

    private Buy buy;

    public BuyCarProxy (Buy buy) {
        this.buy = buy;
    }

    @Override
    public void buyCar() {
        System.out.println("找车源...");
        System.out.println("质检...");
        System.out.println("过户...");
        this.buy.buyCar();
        System.out.println("开回家...");
    }

    public static void main (String[] args) {
        Buy buy = new BuyCar();
        buy.buyCar();
        System.out.println(" ");
        BuyCarProxy proxy = new BuyCarProxy(buy);
        proxy.buyCar();
    }

}

package com.moonligt.designpatterns.designpatterns.chainofresponsibility.filter;

/**
 * @ClassName ScriptFilter
 * @Description: TODO
 * @Author Moonlight
 * @Date 2020/5/16 15:54
 * @Version V1.0
 **/
public class ScriptFilter implements Filter {
    @Override
    public boolean doFilter(Req req, Resp resp, FilterChain chain) {
        String reqMsg = req.getReqMsg();
        reqMsg = reqMsg.replaceAll("script", " script ").replaceAll("<", "[").replaceAll(">", "]");
        req.setReqMsg(reqMsg + " - scriptFilter");

        chain.doFilter(req, resp, chain);

        resp.setRespMsg(resp.getRespMsg() + " - scriptFilter");

        return false;
    }
}

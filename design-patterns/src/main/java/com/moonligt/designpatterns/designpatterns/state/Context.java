package com.moonligt.designpatterns.designpatterns.state;

/**
 * @ClassName Context
 * @Description: 状态管理上下文。
 *               由这个上下文管理状态的迁移，但这样子导致了这个类必须要知道所有的状态有哪些
 *               两者之间会产生很强的依赖性
 * @Author Moonlight
 * @Date 2020/5/16 21:00
 * @Version V1.0
 **/
public class Context {

    public final static OpenState OPEN_STATE = new OpenState();
    public final static CloseState CLOSE_STATE = new CloseState();
    public final static RunState RUN_STATE = new RunState();
    public final static StopState STOP_STATE = new StopState();


    private LiftState state;

    public void setState(LiftState state) {
        this.state = state;
        this.state.setContext(this);
    }

    public LiftState getState() {return this.state;}

    public void open(){
        this.state.open();
    }

    public void close(){
        this.state.close();
    }

    public void run(){
        this.state.run();
    }

    public void stop(){
        this.state.stop();
    }
}
